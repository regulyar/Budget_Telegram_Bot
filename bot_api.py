from telegram.ext import Updater, CommandHandler
import telegram
import logging
import sqlite3


def hello(bot, update):
    update.message.reply_text(
        'Hello {}'.format(update.message.from_user.first_name))


class Budget_Bot(object):
    def __init__(self, database_name="Transactions.db"):
        self.database_name = database_name
        self.database_connection = sqlite3.connect(self.database_name)
        self.database_cursor = self.database_connection.cursor()
        self.database_cursor.execute('''CREATE TABLE IF NOT EXISTS Categories (
          category_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          category_name TEXT NOT NULL,
          user_id INTEGER NOT NULL
        );''')
        self.database_cursor.execute('''CREATE TABLE IF NOT EXISTS Transactions (
          category_id INTEGER NOT NULL,
          amount REAL NOT NULL,
          comment TEXT,
          FOREIGN KEY(category_id) REFERENCES Categories(category_id)
        );''')
        self.database_cursor.execute('''CREATE TRIGGER IF NOT EXISTS category_deleting
                  BEFORE DELETE ON Categories
                  BEGIN 
                    DELETE FROM Transactions
                    WHERE category_id = OLD.category_id;
                  END;''')
        self.updater = Updater('567631278:AAEXMW3o5vGl0b6aFH5fKacDaSxJUcNBX5w',
                               request_kwargs={'proxy_url': 'http://94.177.216.109:8888'})
        self.updater.dispatcher.add_handler(CommandHandler('income', self.add_income))
        self.updater.dispatcher.add_handler(CommandHandler('expense', self.add_expense))
        self.updater.dispatcher.add_handler(CommandHandler('add', self.add_category))
        self.updater.dispatcher.add_handler(CommandHandler('delete', self.delete_category))
        self.updater.dispatcher.add_handler(CommandHandler('overview', self.send_overview))
        self.updater.dispatcher.add_handler(CommandHandler('list', self.show_categories))
        self.updater.dispatcher.add_handler(CommandHandler('all', self.show_all_transactions))
        self.updater.dispatcher.add_handler(CommandHandler('clear', self.clear))
        self.updater.dispatcher.add_handler(CommandHandler('start', self.start))
        self.updater.dispatcher.add_handler(CommandHandler('help', self.help))
        self.action_keyboard = [['/all', '/overview'],
                                ['/list', '/help']]
        self.action_markup = telegram.ReplyKeyboardMarkup(self.action_keyboard, resize_keyboard=True)
        self.database_connection.commit()
        self.database_connection.close()

    def start(self, bot, update):
        update.message.reply_text('Hello, {}! I\'m BudgetBot. '
                                  'Type /help to find out what I can do.'.format(update.message.from_user.first_name))

    def help(self, bot, update):
        update.message.reply_text('''Commands list:
        /help - Shows this message.
        /add <category_name> - Adds new category to your category list with name <category_name>.
        /delete <category_name> - Deletes category with name <category_name> from your category list.
        /income <category_name> <amount> <comment> - Adds income in <category_name> category with, <amount> - amount of money your earned. You can add comment <comment> to transaction.
        /expense <category_name> <amount> <comment> - Adds expense in <category_name> category with, <amount> - amount of money your expanded. You can add comment <comment> to transaction.
        /overview - Shows short information about your incomes and expenses in every category.
        /list - Shows a list of your categories.
        /all - Shows full history of transactions in every category.
        /clear - Clears ALL transactions history (Doesn't change the list of categories).''')
        bot.send_message(chat_id=update.message.chat.id, text='Choose action:', reply_markup=self.action_markup)

    def run(self):
        self.updater.start_polling()
        self.updater.idle()

    def set_connection(self):
        self.database_connection = sqlite3.connect(self.database_name)
        self.database_cursor = self.database_connection.cursor()

    def close_connection(self, bot, update):
        bot.send_message(chat_id=update.message.chat.id, text='Choose action:', reply_markup=self.action_markup)
        self.database_connection.commit()
        self.database_connection.close()

    def add_income(self, bot, update):
        parsed_message = update.message.text.split()
        if len(parsed_message) < 3:
            update.message.reply_text('''Incorrect request. It should be:
"/income <category_name> <amount> <comment (optional)>"''')
            return
        self.set_connection()
        id = update.message.from_user.id
        categories = list(self.database_cursor.execute('''SELECT category_id FROM Categories
                  WHERE user_id = ? AND category_name = ?;''', (id, parsed_message[1].lower())))
        if len(categories) == 0:
            update.message.reply_text('''Add category {} to the category list \
            before adding transactions to it.'''.format(parsed_message[1]))
            self.close_connection(bot, update)
            return
        try:
            amount = float(parsed_message[2])
        except ValueError:
            update.message.reply_text('Wrong amount format: {}. Try again!'.format(parsed_message[2]))
            self.close_connection(bot, update)
            return
        comment = ''
        for i in range(3, len(parsed_message)):
            comment += parsed_message[i]
        self.database_cursor.execute('''INSERT INTO Transactions (category_id, amount, comment) VALUES (
                  ?, ?, ?
                );''', (categories[0][0], amount, comment))
        update.message.reply_text('Income added! +' + str(amount) + ' in {}.'.format(parsed_message[1]))
        self.close_connection(bot, update)

    def add_expense(self, bot, update):
        parsed_message = update.message.text.split()
        if len(parsed_message) < 3:
            update.message.reply_text('''Incorrect request. It should be:
"/expense <category_name> <amount> <comment (optional)>"''')
            return
        self.set_connection()
        id = update.message.from_user.id
        categories = list(self.database_cursor.execute('''SELECT category_id FROM Categories
                          WHERE user_id = ? AND category_name = ?;''', (id, parsed_message[1].lower())))
        if len(categories) == 0:
            update.message.reply_text('Add category {} to the category list'
                                      'before adding transactions to it.'.format(parsed_message[1]))
            self.close_connection(bot, update)
            return
        try:
            amount = float(parsed_message[2])
        except ValueError:
            update.message.reply_text('Wrong amount format: {}. Try again!'.format(parsed_message[2]))
            self.close_connection(bot, update)
            return
        comment = ''
        for i in range(3, len(parsed_message)):
            comment += parsed_message[i]
        self.database_cursor.execute('''INSERT INTO Transactions (category_id, amount, comment) VALUES (
                          ?, ?, ?
                        );''', (categories[0][0], -amount, comment))
        update.message.reply_text('Expense added! -' + str(amount) + ' in {}.'.format(parsed_message[1]))
        self.close_connection(bot, update)

    def add_category(self, bot, update):
        parsed_message = update.message.text.split()
        if len(parsed_message) < 2:
            return
        self.set_connection()
        id = update.message.from_user.id
        if len(list(self.database_cursor.execute('''SELECT category_id FROM Categories
                  WHERE user_id = ? AND category_name = ?;''',
                                                 (id, parsed_message[1].lower())))) != 0:
            update.message.reply_text('This category already exists: {}.'.format(parsed_message[1].lower()))
            self.close_connection(bot, update)
            return
        else:
            self.database_cursor.execute('''INSERT INTO Categories (category_name, user_id) VALUES (
                  ?, ?
                );''', (parsed_message[1].lower(), id))
            update.message.reply_text('Category {} added!'.format(parsed_message[1].lower()))
            self.close_connection(bot, update)

    def delete_category(self, bot, update):
        parsed_message = update.message.text.split()
        if len(parsed_message) < 2:
            return
        self.set_connection()
        id = update.message.from_user.id
        if len(list(self.database_cursor.execute('''SELECT category_id FROM Categories
                          WHERE user_id = ? AND category_name = ?;''',
                                                 (id, parsed_message[1].lower())))) == 0:
            update.message.reply_text('No such category to delete: {}.'.format(parsed_message[1].lower()))
            self.close_connection(bot, update)
            return
        else:
            self.database_cursor.execute('''DELETE FROM Categories
                          WHERE user_id = ? AND category_name = ?;''', (id, parsed_message[1].lower()))
            update.message.reply_text('Category {} deleted!'.format(parsed_message[1].lower()))
            self.close_connection(bot, update)

    def send_overview(self, bot, update):
        self.set_connection()
        id = update.message.from_user.id
        incomes_text = '*Your incomes:*\n'
        for category in list(self.database_cursor.execute('''SELECT category_id, category_name FROM Categories
                          WHERE user_id = ?;''', (id, ))):
            incomes_text += category[1] + ': '
            incomes_text += str(list(self.database_cursor.execute('''SELECT sum(amount)
                          FROM Transactions
                          WHERE category_id = ? AND amount > 0;''', (category[0], )))[0][0] or 0) + '\n'
        update.message.reply_text(incomes_text, parse_mode='Markdown')
        expenses_text = '*Your expenses:*\n'
        for category in list(self.database_cursor.execute('''SELECT category_id, category_name FROM Categories
                                  WHERE user_id = ?;''', (id, ))):
            expenses_text += category[1] + ': '
            expenses_text += str(-(list(self.database_cursor.execute('''SELECT sum(amount)
                                  FROM Transactions
                                  WHERE category_id = ? AND amount < 0;''', (category[0], )))[0][0] or 0)) + '\n'
        update.message.reply_text(expenses_text, parse_mode='Markdown')
        self.close_connection(bot, update)

    def show_categories(self, bot, update):
        self.set_connection()
        id = update.message.from_user.id
        categories_text = '*Your categories:*\n' \
                          '===============\n'
        for category in list(self.database_cursor.execute('''SELECT category_name FROM Categories
                          WHERE user_id = ?;''', (id, ))):
            categories_text += category[0] + '\n'
        update.message.reply_text(categories_text, parse_mode='Markdown')
        self.close_connection(bot, update)

    def show_all_transactions(self, bot, update):
        self.set_connection()
        id = update.message.from_user.id
        incomes_text = '*Your incomes:*\n' \
                       '===============\n'
        for category in list(self.database_cursor.execute('''SELECT category_id, category_name FROM Categories
                          WHERE user_id = ?;''', (id, ))):
            incomes_text += '*{} :*\n'.format(category[1])
            for transaction in list(self.database_cursor.execute('''SELECT amount, comment FROM Transactions
                          WHERE category_id = ? AND amount > 0;''', (category[0], ))):
                incomes_text += '           ' + str(transaction[0])
                if isinstance(transaction[1], str) and transaction[1] != '':
                    incomes_text += ' - ' + transaction[1]
                incomes_text += '\n'
            incomes_text += '===============\n'
        update.message.reply_text(incomes_text, parse_mode='Markdown')
        expenses_text = '*Your expenses:*\n' \
                        '===============\n'
        for category in list(self.database_cursor.execute('''SELECT category_id, category_name FROM Categories
                                  WHERE user_id = ?;''', (id,))):
            expenses_text += '*{} :*\n'.format(category[1])
            for transaction in list(self.database_cursor.execute('''SELECT amount, comment FROM Transactions
                                  WHERE category_id = ? AND amount < 0;''', (category[0], ))):
                expenses_text += '          ' + str(-transaction[0])
                if isinstance(transaction[1], str) and transaction[1] != '':
                    expenses_text += ' - ' + transaction[1]
                expenses_text += '\n'
            expenses_text += '===============\n'
        update.message.reply_text(expenses_text, parse_mode='Markdown')
        self.close_connection(bot, update)

    def clear(self, bot, update):
        self.set_connection()
        id = update.message.from_user.id
        for category in list(self.database_cursor.execute('''SELECT category_id FROM Categories
                                          WHERE user_id = ?;''', (id,))):
            print(category[0])
            self.database_cursor.execute('''DELETE FROM Transactions WHERE category_id = ?''', (category[0], ))
        self.close_connection(bot, update)
        update.message.reply_text('All transactions deleted!')


logging.basicConfig(level=logging.DEBUG)
bot = Budget_Bot("Transactions.db")
bot.run()
